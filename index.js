"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = sample;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function sample() {
  return /*#__PURE__*/_react["default"].createElement("div", null, "I am a dummy react npm module - VTS ");
}
